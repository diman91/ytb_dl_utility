/*
 * Author : dimos katsiamrdos
 * Date   : 2019/08/03
 * Info   : Create a youtube-dl GUI using Qt (Cute) Creator and linux youtube-dl utility
 *          Developed under Ubuntu 18.04 LTS | Qt Creator Open SOurce Community Edition
 */
#include "youtube_dl_dialog.h"
#include "ui_youtube_dl_dialog.h"

Youtube_dl_Dialog::Youtube_dl_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Youtube_dl_Dialog)
{
    ui->setupUi(this);
    set_pixmaps();
    initialize();
}

Youtube_dl_Dialog::~Youtube_dl_Dialog()
{
    delete ui;
}

void Youtube_dl_Dialog::set_pixmaps()
{
    // Create a QPixMap Object | You have to specify your logo image path here
    QPixmap pix(":/beta_logo.jpg");
    int width = ui->label_beta->width();
    int height = ui->label_beta->height();
    ui->label_beta->setPixmap(pix.scaled(width, height, Qt::KeepAspectRatio));

    QPixmap audio(":/mp3.png");
    QPixmap video(":/mkv.png");
    QPixmap exit(":/exit.png");
    ui->audio_label->setPixmap(audio.scaled(ui->audio_label->width(), ui->audio_label->height(), Qt::KeepAspectRatio));
    ui->video_label->setPixmap(video.scaled(ui->video_label->width(), ui->video_label->height(), Qt::KeepAspectRatio));
    ui->exit_label->setPixmap(exit.scaled(ui->exit_label->width(), ui->exit_label->height(), Qt::KeepAspectRatio));
}

void Youtube_dl_Dialog::initialize()
{
    // Create a QProcess Object to capture the output of Linux commands
    /* Print info about where this GUI is running using QProcess widget
     * QString username = id -un
     * QString hostname = hostname
     * QString date     = date
    */
    QProcess id, date, hostname;
    QString id_output, date_output, hostname_output; //time_output;
    QString AIO;
    id.start("id -un");
    id.waitForFinished();
    id_output = id.readAllStandardOutput();

    date.start("date +%Y/%m/%d");
    date.waitForFinished();
    date_output = date.readAllStandardOutput();

    hostname.start("hostname");
    hostname.waitForFinished();
    hostname_output = hostname.readAllStandardOutput();

    // Output all this info to the system_label
    AIO = "HOST : " + hostname_output + "\nUSER : " + id_output + "\nDATE : " + date_output;
    ui->system_label->setText(AIO);
}

// The function to download the youtube_url QString and save it to
// the path_url Directory
bool Youtube_dl_Dialog::download_audio()
{
    QString cmd = "youtube-dl --extract-audio --audio-format mp3 ";
    cmd = "cd " + path_url + " && " + cmd + youtube_url + " && echo $?";
    int exitStatus = system(cmd.toStdString().c_str());
    return exitStatus==0 ? true : false;
}

// The fucntion to download the youtube_url QString and
// save it to the path_url Directory
bool Youtube_dl_Dialog::download_video()
{
    QString cmd = "youtube-dl ";
    cmd = "cd " + path_url + " && " + cmd + youtube_url + " && echo $?";
    int exitStatus = system(cmd.toStdString().c_str());
    return exitStatus==0 ? true : false;
}

QString Youtube_dl_Dialog::getPath_url() const
{
    return path_url;
}

QString Youtube_dl_Dialog::getYoutube_url() const
{
    return youtube_url;
}


void Youtube_dl_Dialog::on_ok_pushButton_clicked()
{
    // Set the youtube_dl url QString to the global variable
    setYoutube_url(ui->youtube_url_liner->text());
    // Set the path_url QString to the global variable
    setPath_url(ui->path_liner->text());

    // Fetch the url title after user has entered the url of course!
    FetchTitle();

    // The return code when the command is executed | 0-> command is executed successfully
    // 1 --> Command failed to execute
    bool return_code;

    if(ui->exit_radioButton->isChecked()) {
        QMessageBox::information(this, "EXIT", "Nothing to do!");
        accept();
        return;
    }

    // If the url or the path is empty open a message box and display a message
    if(youtube_url.isEmpty()) {
        QMessageBox::information(this, "FAILURE", "Youtube URL should not be empty!");
    }

    if(path_url.isEmpty()) {
        QMessageBox::information(this, "FAILURE", "Path should not be empty!");
    }

    // Next, if youtube url && saved path string are not empty, then proceed
    if(!youtube_url.isEmpty() && !path_url.isEmpty()) {

        // Next, if this OK Button is clicked, since the youtube-url and the path are not empty
        // we can proceed based on the choice the user chose : [ mp3 | mkv | exit ]
        // Action I :: Download mp3 format
        if(ui->mp3_radioButton->isChecked()) {
            // Download the youtube-url in mp3 format
            return_code = download_audio();
            if(return_code==true) {
                // Concole information prit-out
                QMessageBox::information(this, "INFO", "Song " + url_title + " download completed!");
            } else {
                QMessageBox::warning(this, "WARN", "Song " + url_title + " download failed!");
            }
        }
        // Action II :: download mkv | video format
        if(ui->video_radioButton->isChecked()) {
            // Download the youtube-url in video format
            return_code = download_video();
            if(return_code==true) {
                QMessageBox::information(this, "INFO", "Video download completed!");
            }
            else {
                QMessageBox::warning(this, "WARN", "Video " + url_title + " download failed!");
            }
        }
        if(ui->exit_radioButton->isChecked()) {
            accept();
        }
    }
    // When audio | video format is downloaded , retur,m aybe the operator
    // would like to download another one!
    return;
}

void Youtube_dl_Dialog::on_cancel_pushButton_clicked()
{
    qDebug() << "Cancel Button is pushed ... Aborting Youtube-dl GUI Utility ... \n";
    // If the user pushes the EXIT button on chance we have to ask her/him one last time if
    // she/he would like to exit or not
    int checked_action = QMessageBox::question(this, "Warning Message", "Are you sure you wish to exit the application?", QMessageBox::No | QMessageBox::Yes);

    if(checked_action==QMessageBox::Yes) {
        reject();
    } else  if(checked_action==QMessageBox::No) {
        return;
    }

    //reject();
}

void Youtube_dl_Dialog::on_info_button_clicked()
{
    QMessageBox::information(this, "INFO", "This is a simple Youtube-dl GUI to help You download a url in audio or video format! Enter url, path, choose format and press PROCEED.");
}

void Youtube_dl_Dialog::on_clear_pushButton_clicked()
{
    // If the CLEAR button is pressed clear the youtube url and the path fields
    // Also, reset the appropraite QStrings to ""
    ui->youtube_url_liner->setText("");
    ui->path_liner->setText("");
    setPath_url("");
    setYoutube_url("");
    // Also, uncheck any radio button, in case it is clicked
    if(ui->mp3_radioButton->isChecked()) {
        ui->mp3_radioButton->setAutoExclusive(false);
        ui->mp3_radioButton->setChecked(false);
        ui->mp3_radioButton->setAutoExclusive(true);
    }
    if(ui->video_radioButton->isChecked()) {
        ui->video_radioButton->setAutoExclusive(false);
        ui->video_radioButton->setChecked(false);
        ui->video_radioButton->setAutoExclusive(true);
    }
    if(ui->exit_radioButton->isChecked()) {
        ui->exit_radioButton->setAutoExclusive(false);
        ui->exit_radioButton->setChecked(false);
        ui->exit_radioButton->setAutoExclusive(true);
    }
    if(ui->print_title_checkBox->isChecked()) {
        ui->print_title_checkBox->setAutoExclusive(false);
        ui->print_title_checkBox->setChecked(false);
        ui->print_title_checkBox->setAutoExclusive(true);
    }
    if(ui->check_if_installed_checkBox->isChecked() ) {
        ui->check_if_installed_checkBox->setAutoExclusive(false);
        ui->check_if_installed_checkBox->setChecked(false);
        ui->check_if_installed_checkBox->setAutoExclusive(true);
    }
    if(ui->check_ytb_dl_version_checkBox->isChecked()) {
        ui->check_ytb_dl_version_checkBox->setAutoExclusive(false);
        ui->check_ytb_dl_version_checkBox->setChecked(false);
        ui->check_ytb_dl_version_checkBox->setAutoExclusive(true);
    }
    return;
}

void Youtube_dl_Dialog::setYoutube_url(const QString &value)
{
    youtube_url = value;
}

void Youtube_dl_Dialog::setPath_url(const QString &value)
{
    path_url = value;
}

void Youtube_dl_Dialog::on_Select_pushButton_clicked()
{
    QString SelDirDialog = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                                  "/home", QFileDialog::ShowDirsOnly
                                                                  | QFileDialog::DontResolveSymlinks);
    ui->path_liner->setText(SelDirDialog);
    setPath_url(SelDirDialog);
}

/*
 * Update: 12/08/2019
 * Check if youtube-dl command is installed in Linux
 * If it is not, give the ability to the user to install it
 * It is going to be a vital utility for this GUI application to
 * run correctly and download url's
 */

// Check if youtube-dl is installed
void Youtube_dl_Dialog::on_check_if_installed_checkBox_clicked(bool checked)
{
    if(checked==true)
    {
        // Simple use a QProcess to run a which command upon youtube-dl command
        // If this command retyurns true, i.e. 0, youtube-dl is installed
        // Other wise, if 1 is returned, youtube-dl is not installed
        QProcess ytb_proc;
        ytb_proc.start("which youtube-dl");
        ytb_proc.waitForFinished(5000);
        if(ytb_proc.exitCode()!=0)
        {
            QMessageBox::information(this, "youtube-dl status", "Youtube-dl utility is not installed!");
            is_ytb_dl_installed = false;
        } else if(ytb_proc.exitCode()==0)
        {
            QMessageBox::information(this, "youtube-dl status", "Youtube-dl utility is installed!");
            is_ytb_dl_installed = true;
        }
    }
}

// Find the version of the youtube-dl
void Youtube_dl_Dialog::on_check_ytb_dl_version_checkBox_clicked(bool checked)
{
    if(checked==true) {
        QProcess version_proc;
        version_proc.start("bash", QStringList() << "-c" << " youtube-dl --version ");\
        if(!version_proc.waitForFinished(3000)) {
            return;
        }
        QString youtube_dl_version(version_proc.readAllStandardOutput());
        QMessageBox::information(this, "Youtube-dl command Version", youtube_dl_version);
    }
}

// Sub-functions needed for the installation of youtube-dl command when
// the button is pressed
bool Youtube_dl_Dialog::get_youtube_dl()
{
    QProcess passwd_proc, install_ytb_proc;
    passwd_proc.setStandardOutputProcess(&install_ytb_proc);
    passwd_proc.start("echo " + password);
    install_ytb_proc.start("sudo -S curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl");
    if(!passwd_proc.waitForFinished() || !install_ytb_proc.waitForFinished())
    {
        QMessageBox::critical(this, "ERROR", "The youtube-dl failed to download!");
        return false;
    }

    if(install_ytb_proc.exitCode()!=0)
    {
        QMessageBox::critical(this, "ERROR", "youtube-dl returned exit code 1");
        return false;
    }

    return true;
}

bool Youtube_dl_Dialog::make_executable()
{
    QProcess passwd_proc, exec_proc;
    passwd_proc.setStandardOutputProcess(&exec_proc);
    passwd_proc.start("echo " + password);
    exec_proc.start("sudo -S chmod a+rx /usr/local/bin/youtube-dl");
    if(!passwd_proc.waitForFinished() || !exec_proc.waitForFinished())
    {
        QMessageBox::critical(this, "ERROR", "Making youtube-dl executable failed!");
        return false;
    }

    if(exec_proc.exitCode()!=0) {
        QMessageBox::critical(this, "ERROR", "chmod process returned exit code 1");
        return false;
    }

    return true;
}

/* Install the youtube-dl command in Linux using next commands, as shown in github official page of youtube-dl :
 * sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
 * sudo chmod a+rx /usr/local/bin/youtube-dl
 * Open a dialog and ask the user for its password
 */
void Youtube_dl_Dialog::on_install_ytb_dl_Button_clicked()
{
    on_check_if_installed_checkBox_clicked(true);
    bool pass_entered;

    if(is_ytb_dl_installed==true) {
        QMessageBox::information(this, "INFO", "No need! Youtube-dl is already installed in your system!");
        return;
    }

    password = QInputDialog::getText(nullptr, "Installing youtube-dl", "Please enter your password: ",
                                             QLineEdit::Password, "", &pass_entered);
    if(pass_entered==true && !password.isEmpty())
    {
        // Here I have the password | I can install youtube-dl !
        // Create a process with pipes
        if(get_youtube_dl()==true && make_executable()==true)
        {
            QMessageBox::information(this, "INFO", "Youtube-dl installed successfully");
            return;
        }
        else {
            QMessageBox::critical(this, "ERROR", "Youtube-dl failed to be installed! Please try manually!");
            return;
        }
    }
    return;
}


/*
 * Remove the youtube-dl Linuc command using the well known command :
 * sudo apt-get remove youtube-dl
 */

// Subfunction used by the remove's pushButton 'main' function on_remove_ytb_dl_Button_clicked()
// As usual use a QProcess to run Linux commands requiring password for sudo authentication
// Bug Fix : 15/08/2019 :: Forgot to show prompt for user's password when ininstalling youtube-dl
bool Youtube_dl_Dialog::remove_youtube_dl()
{
    bool pass_entered;
    password = QInputDialog::getText(nullptr, "Removing youtube-dl", "Please enter your password: ",
                                             QLineEdit::Password, "", &pass_entered);
    if(pass_entered==false || password.isEmpty())
    {
        QMessageBox::critical(this, "ERROR", "Please enter your password and try again! No empty password accepted!");
        return false;
    }

    QProcess passwd_proc, remove_proc;
    passwd_proc.setStandardOutputProcess(&remove_proc);
    passwd_proc.start("echo " + password);
    remove_proc.start("sudo -S apt-get remove youtube-dl");
    if(!passwd_proc.waitForFinished() || !remove_proc.waitForFinished())
    {
        QMessageBox::critical(this, "ERROR", "The youtube-dl failed to be removed!");
        return false;
    }
    if(remove_proc.exitCode()!=0)
    {
        QMessageBox::critical(this, "ERROR", "Youtube-dl remove process returned exit code 1!");
        return false;
    }

    return true;
}

/*
 * Remove youtube-dl Linux command 'remove_ytb_dl_Button' pushButton
 * Remove it only if it is installed. Otherwise, no need to even start this
 * action of removal of this package
 */
void Youtube_dl_Dialog::on_remove_ytb_dl_Button_clicked()
{
    on_check_if_installed_checkBox_clicked(true);

    if(is_ytb_dl_installed==false)
    {
        QMessageBox::information(this, "INFO", "No need to remove youtube-dl. It is not installed!");
        return;
    }
    else {
       if(remove_youtube_dl()==true)
       {
           return;
       }
    }
}

// Update II : Add update functionality for youtube-dl Linux command
bool Youtube_dl_Dialog::update_youtube_dl()
{
    bool pass_entered;
    password = QInputDialog::getText(nullptr, "Updating youtube-dl", "Please enter your password: ",
                                             QLineEdit::Password, "", &pass_entered);
    if(pass_entered==false || password.isEmpty())
    {
        QMessageBox::critical(this, "ERROR", "Please enter your password and try again! No empty password accepted!");
        return false;
    }

    QProcess passwd_proc, update_proc;
    passwd_proc.setStandardOutputProcess(&update_proc);
    passwd_proc.start("echo " + password);
    update_proc.start("sudo -S youtube-dl --update");
    if(!passwd_proc.waitForFinished() || !update_proc.waitForFinished())
    {
        QMessageBox::critical(this, "ERROR", "The youtube-dl failed to be updated!");
        return false;
    }
    if(update_proc.exitCode()!=0)
    {
        QMessageBox::critical(this, "ERROR", "Youtube-dl update process returned exit code 1!");
        return false;
    }

    return true;
}


// Fetches the youtube url title from the site page
void Youtube_dl_Dialog::FetchTitle()
{
    QProcess title_proc;
    title_proc.start("youtube-dl --get-filename " + youtube_url);
    if(!title_proc.waitForFinished(5000) || title_proc.exitStatus()!=0)
    {
        url_title = "[]";
    }
    else {
        url_title = title_proc.readAllStandardOutput();
        url_title.remove("\n");
    }
}

void Youtube_dl_Dialog::on_update_ytb_dl_Button_clicked()
{
    // This command will update youtube-dl to latest version only if it is already installed
    on_check_if_installed_checkBox_clicked(true);

    if(is_ytb_dl_installed==false)
    {
        QMessageBox::information(this, "INFO", "Youtube-dl is not installed. Cannot update it!");
        return;
    }
    else {
       if(update_youtube_dl()==true)
       {
           // Pretend I clicked the check version button in order to show the updated youtube-dl version
           on_check_ytb_dl_version_checkBox_clicked(true);
           return;
       }
    }
}


void Youtube_dl_Dialog::on_print_title_checkBox_clicked(bool checked)
{
    setYoutube_url(ui->youtube_url_liner->text());
    if(youtube_url.isEmpty()) {
        QMessageBox::warning(this, "WARNING", "Please provide a url first!");
        return;
    }
    if(checked==true) {
        FetchTitle();
        if(!url_title.isEmpty()){
            QMessageBox::information(this, "TITLE", url_title);
        }
    }
}
