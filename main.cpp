/*
 * Author : dimos katsiamrdos
 * Date   : 2019/08/03
 * Info   : Create a youtube-dl GUI using Qt (Cute) Creator and linux youtube-dl utility
 *          Developed under Ubuntu 18.04 LTS | Qt Creator Open SOurce Community Edition
 */
#include "widget.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>

QString readFile(QString filepath)
{
    QFile in_file(filepath);
    if(in_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&in_file);
        return in.readAll();
    }
    return "";
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString my_style=readFile(":/stylesheets/mystyle.css");
    if(my_style.length()>0) {
        a.setStyleSheet(my_style);
    }
    Widget w;
    w.show();

    return a.exec();
}
