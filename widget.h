/*
 * Author : dimos katsiamrdos
 * Date   : 2019/08/03
 * Info   : Create a youtube-dl GUI using Qt (Cute) Creator and linux youtube-dl utility
 *          Developed under Ubuntu 18.04 LTS | Qt Creator Open SOurce Community Edition
 */
#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QApplication>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_starter_button_clicked();

    void on_quit_app_pushButton_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
