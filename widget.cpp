/*
 * Author : dimos katsiamrdos
 * Date   : 2019/08/03
 * Info   : Create a youtube-dl GUI using Qt (Cute) Creator and linux youtube-dl utility
 *          Developed under Ubuntu 18.04 LTS | Qt Creator Open SOurce Community Edition
 */
#include "widget.h"
#include "ui_widget.h"
#include "youtube_dl_dialog.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_starter_button_clicked()
{
    // First, let us create the youtube-dl dialog class object
    Youtube_dl_Dialog *dialog = new Youtube_dl_Dialog(this);
    // Fire it up
    dialog -> exec();
}

void Widget::on_quit_app_pushButton_clicked()
{
    QApplication::quit();
}
