/*
 * Author : dimos katsiamrdos
 * Date   : 2019/08/03
 * Info   : Create a youtube-dl GUI using Qt (Cute) Creator and linux youtube-dl utility
 *          Developed under Ubuntu 18.04 LTS | Qt Creator Open SOurce Community Edition
 */
#ifndef YOUTUBE_DL_DIALOG_H
#define YOUTUBE_DL_DIALOG_H

#include <QDialog>

#include <QDebug>             // For debug messages diaplyed in the Qt Creator console
#include <QMessageBox>       //  For Message boxes
#include <stdlib.h>         //   For system() function
#include <QPixmap>         //    For the beta version image logo
#include <QProcess>       //     For the left upper label information label | to be filled by this Object functions
#include <QFileDialog>   //      For the selection of the path in case the user does not want to type the path
#include <QInputDialog>

namespace Ui {
class Youtube_dl_Dialog;
}

class Youtube_dl_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Youtube_dl_Dialog(QWidget *parent = nullptr);
    ~Youtube_dl_Dialog();
    // Setter for saved path Directory string
    void setPath_url(const QString &value);
    // Setter for youtube url string
    void setYoutube_url(const QString &value);
    // Getter for youtube url string
    QString getYoutube_url() const;
    // getter for saved path directory string
    QString getPath_url() const;
    // Getter & Setter for the url title
    QString getUrl_title() const;
    void setUrl_title(const QString &value);

private slots:
    // Proceed QPushButton
    void on_ok_pushButton_clicked();
    // Cancel the program
    void on_cancel_pushButton_clicked();
    // Print some help info for this program
    void on_info_button_clicked();
    // Clear all fields QPushButton
    void on_clear_pushButton_clicked();
    // PushButton for selecting the saved path directory
    void on_Select_pushButton_clicked();
    // bool subfunction for downlaoad url in audio format
    bool download_audio();
    // Same for video format
    bool download_video();
    // Check if youtube-dl is installed
    void on_check_if_installed_checkBox_clicked(bool checked);
    // Check youtube-dl version
    void on_check_ytb_dl_version_checkBox_clicked(bool checked);
    // Install youtube-dl
    void on_install_ytb_dl_Button_clicked();
    // SUbfunction to download / curl the youtube-dl using QProcess
    bool get_youtube_dl();
    // SUbfunction to make youtube-dl executable using a QProcess
    bool make_executable();
    // Remove youtube-dl QProcess
    bool remove_youtube_dl();
    // Remove youtube-dl PushButton
    void on_remove_ytb_dl_Button_clicked();
    // Set pixmaps for download options audio | video or exit
    void set_pixmaps();
    // Initialize help function
    void initialize();
    // Update youtube-dl QPushButton
    void on_update_ytb_dl_Button_clicked();
    // Update youtube-dl subfunction using QProcess
    bool update_youtube_dl();
    // A function that returns the url title in QString format using a QProcess
    void FetchTitle();

    void on_print_title_checkBox_clicked(bool checked);

private:
    // Let us define a string for storing the entered youtube url
    // from the user that will be used by the line edit widget
    QString youtube_url;
    // Let us also define a string QString actually, that will capture
    // the path that the user will enter in order to save his url song or whatever !
    QString path_url;
    Ui::Youtube_dl_Dialog *ui;
    /* Update youtube-dl Utility :
     * 12/08/2019 : dimkatsi91
     * Give the ability to the user to check if youtube-dl is installed
     * Also, the same for ffmepg | to be added also next days
     */
    bool is_ytb_dl_installed = false;
    QString password;
    // The title of the youtube url
    QString url_title;

};

#endif // YOUTUBE_DL_DIALOG_H
