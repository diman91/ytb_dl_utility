# Youtube-dl GUI Utility 

Title  : Youtube-dl GUI

Author : Dimos Katsimardos

Date   : August 03, 2019

Dev Environment  : Qt Creator 4.9.2 Open Source Community Edition

Work Environment : Ubuntu Linux 18.04 LTS

GUI Version      : v1.0.5
  
GUI Linux Application Update 12/08/2019 : AppImage Application created in Ubuntu 18.04 OS (Debian-based distros supported)

Update I | 13/08/2019 : Added functionality to check if youtube-dl is installed, to install it & check the version of the youtube-dl command

-------------------------------------------------------------------------------------------------------------------------------

## Information

This GUI is supported under next prerequisites:

* Linux | Mostly Debian-based Distros
* ffmpeg or similar utility needs to be installed for the audio | video download option
* youtube-dl program needs to be installed ( can be installed through this application after Update I )
* Qt Creator 4.9.2 is recommended to be installed in order to open this GUI. You can also run directly the AppImage executable, since Qt is not prerequisite in this case
* After opening the main GUI window select the button in order to start the QDialog regarding Youtube-dl GUI or Exit the application
* Beta version, will be updated frequently for more functionality | still a working project


#### Example : Audio download

- The starting window is appearing as soon the Qt Run button is pressed :

![Starting Window/Widget](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/Start_Window.png)

- Next, when the button 'Start Youtube-dl Utility' is pressed, the Main QDialog GUI is shown:

#### OPTIONS/SCENARIOS
- Provide the youtube url
- Provide the full path
- Enter mp3 || mkv download option
- Press PROCEED
- If CANCEL button is pressed the QDialog window is closed and the main window is coming in focus again to try again!
- If INFO button is pressed a QMessage window appears with a small usage message.
- If a path is not entered, then a Message Window appears and You should start again the application.
- If a url is not entered, then a Message Window appears and You should start again the application too!
- If the provided path does not exist, a Messsage Error Window appears & You should provide a valid path to try again!
- The path for the youtube-dl url save destination can be selected by pressing the 'Select' button, which opens a File Dialog Explorer for the user to choose from.

  ![Youtube-dl Utility GUI](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/The_Main_Dialog_Window.png)

- Enter the youtube url, enter manually the saved directory path or press Select button and select a path from your computer. Then, press PROCEED button in order to trigger the download procedure:

![url, directory and format selection](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/audio_example_i.png)

- The url is being downloaded. As soon as it is completed the next success message will be shown:

  ![Success](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/audio_example_ii.png)

- After completing a download, you can clear the url and directory paths using the CLEAR button.

- Checking whether youtube-dl is installed:

![Check if installed](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/check_if_installed.png)

- Checking the version of Linux youtube-dl command:

![Version of youtube-dl](https://bitbucket.org/diman91/ytb_dl_utility/raw/16edab8c92ee942fb4d0d22e799369405a8b52b2/images/check_version.png)
